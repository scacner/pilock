//
//  CredentialsView.swift
//  piLOCK
//
//  Created by Steven Cacner, Jr. on 5/24/16.
//  Copyright © 2016 Cacs. All rights reserved.
//

import UIKit

class CredentialsView: UIViewController {
    
    @IBOutlet var cancelButton: UIButton!
    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!
    @IBOutlet var serverAddress: UIButton!
    
    let store = UserDefaults.standard
    var cUsername: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Variable to hold server address information
        let host = self.store.value(forKey: "serverAddress") as? String
        
        // Variable to hold username information
        let username = self.store.value(forKey: "username") as? String
        
        // Variable to hold password information
        let password = self.store.value(forKey: "password") as? String
        
        // When CredentialView is loaded, check if there is a stored value for "username". If there is no value, set cursor to username field and present keyboard.
        if (nil == username) {
            self.usernameTextField.becomeFirstResponder()
        } // Else, autofill username and password fields with stored value for "username" and a placeholder in password field. This is a security feature. Also, save current username to "cUsername" variable.
        else {
            usernameTextField.text = username
            passwordTextField.text = "********"
            
            // TODO: Remove these fields once rpi image is created:
            print("Username: ", username ?? "nil")
            print("Password: ", password ?? "nil")
        }
        
        // If there is a current server address, display server address as text for button
        if (nil != host) {
            serverAddress.setTitle(host, for: .normal)
        }
        
        // Hide Cancel button if there are no stored values.
        
        
        // Looks for a tap.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(CredentialsView.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    // Call function when tap is recognized.
    @objc func dismissKeyboard() {
        // Causes the view to resign the first responder status.
        view.endEditing(true)
    }
    
    // Navigate keyboard to next text fields.
    @objc func textFieldShouldReturn(_ textfield: UITextField) -> Bool {
        
        // If on username text field, move to password text field
        if (self.usernameTextField == textfield) {
            self.passwordTextField.becomeFirstResponder()
        } // Else, password text field is already first responder, certain information may be saved, keyboard will be dismissed, and the CredentialsView will be dismissed
        else {
            // Check if username has been changed, if so then save new username
            if (usernameTextField.text != cUsername) {
                store.setValue(usernameTextField.text, forKey: "username")
                store.synchronize()
            }
            // Check if password has been changed, if so then save new password
            if (passwordTextField.text != "********") {
                store.setValue(passwordTextField.text, forKey: "password")
                store.synchronize()
            }
            textfield.resignFirstResponder()
            self.performSegue(withIdentifier: "cancel", sender: self)
        }
        
        return false // To prevent UITextField from inserting line-breaks.
    }
    
    // When button is pressed, show an alert that allows user to modify server address
    @IBAction func setServer(_ sender: AnyObject) {
        
        // Variable for textfield to obtain server address
        var serverAddressTextField: UITextField?
        
        // Variable for key of/to store server address
        let serverAddressStore = self.store.value(forKey: "serverAddress") as? String
        
        // Variable for alert title and message
        let serverAlert = UIAlertController(title: "Lock Server", message: "Enter the lock server's address:", preferredStyle: .alert)
        
        // Variable for cancel action
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            // Do nothing
        }
        
        // Add cancel action to alert
        serverAlert.addAction(cancelAction)
        
        // Variable for save action
        let saveAction = UIAlertAction(title: "Save", style: .default) { (action) in
            // As long as the textfield is not empty, save the text in textfield as the server address and change button in CredentialsView to match updated server address
            if (nil != serverAddressTextField?.text && "" != serverAddressTextField?.text) {
                self.store.setValue(serverAddressTextField?.text, forKey:"serverAddress")
                self.store.synchronize()
                self.serverAddress.setTitle(self.store.value(forKey: "serverAddress") as? String, for: .normal)
            }
        }
        
        // Force the save action to be disabled at first
        saveAction.isEnabled = false
        
        // Add save action to alert
        serverAlert.addAction(saveAction)
        
        // Add textfield to alert
        serverAlert.addTextField { (textField) -> Void in
            // Enter the textfield customization code here.
            serverAddressTextField = textField
            serverAddressTextField?.placeholder = "Server Address"
            serverAddressTextField?.keyboardType = .URL
            serverAddressTextField?.returnKeyType = .done
            serverAddressTextField?.enablesReturnKeyAutomatically = true
            
            // When the text in the textfield changes, enable the save action
            NotificationCenter.default.addObserver(forName: NSNotification.Name.UITextFieldTextDidChange, object: serverAddressTextField, queue: OperationQueue.main) { (notification) in
                saveAction.isEnabled = true
            }
        }
        
        // Present the alert with animation
        self.present(serverAlert, animated: true) {
            // If there is a stored server address, autofill textfield with server address and enable save action
            if (nil != serverAddressStore) {
                serverAddressTextField?.text = serverAddressStore
                saveAction.isEnabled = true
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
