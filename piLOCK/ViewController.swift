//
//  ViewController.swift
//  piLOCK
//
//  Created by Steven Cacner, Jr. on 5/22/16.
//  Copyright © 2016 Cacs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var settings: UIButton!
    @IBOutlet var doorStatusText: UILabel!
    @IBOutlet var doorImage: UIImageView!
    @IBOutlet var testConnectionLabel: UILabel!
    
    let store = UserDefaults.standard
    
    var ssh: NMSSHSession!
    var response: String!
    
    let sshQueue = DispatchQueue(label: "com.cacs.sshqueue", qos: DispatchQoS.userInitiated)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // Change image of door based on doorStatusText
        if (doorStatusText.text == "Door Open") {
            doorImage.image = UIImage(named: "open.jpg")
        } else {
            doorImage.image = UIImage(named: "close.jpg")
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
        
        // Check that there is a valid host, username, and password variable
        if self.store.value(forKey: "serverAddress") as? String == nil || self.store.value(forKey: "username") as? String == nil || self.store.value(forKey: "password") as? String == nil {
            // Create alert controller
            let alertController = UIAlertController(title: "Complete Setup", message: "It looks like you're running the app for the first time. Tap the button below to enter the lock server information.", preferredStyle: .alert)
            
            // Add default action to alert
            alertController.addAction(UIAlertAction(title: "Start Setup", style: .default, handler: {(action:UIAlertAction!)-> Void in
                self.performSegue(withIdentifier: "settings", sender: self)}))
            
            // Present alert
            self.present(alertController, animated: true, completion: nil)
        }
    }

    func startSSH(host: String, username: String, password: String) -> NMSSHSession {
        // Start SSH session
        let session = NMSSHSession(host: host, andUsername: username) as NMSSHSession
        session.connect()
        
        // Check if session is connected...
        if session.isConnected == true {
            // ...if connected, use password to authenticate
            session.authenticate(byPassword: password)
            
            // Check is session has been authorized...
            if session.isAuthorized == true {
                // ...if so print success message and change status label
                print("Authentication succeeded")
                //testConnectionLabel.text = "Connected"
            } else {
                // ...else change status label to reflect failure
                //testConnectionLabel.text = "Failure"
            }
        } else {
            // Print error message
            DispatchQueue.main.async {
                self.testConnectionLabel.text = "Connection Failed"
            }
        }
        //TO-DO: What happens if it doesn't connect?
        return session
    }
    
    // Function to end SSH session
    func endSSH(session: NMSSHSession) {
        session.disconnect()
        print("Disonnected")
    }
    
    // Function that will control the testConnection actions of connecting to lock server and controlling door lock for five seconds, as well as control the status label beneath Test button
    func testConnectionAction() {
        // Add synchronous tasks to sshQueue
        sshQueue.async {
            // Add asynchronous task to main queue
            DispatchQueue.main.async {
                // Change status label
                self.testConnectionLabel.text = "Connecting..."
            }
        
            // Variable to hold server address information
            let host = self.store.value(forKey: "serverAddress") as? String
            
            // Variable to hold username information
            let username = self.store.value(forKey: "username") as? String
            
            // Variable to hold password information
            let password = self.store.value(forKey: "password") as? String
            
            // Start SSH session
            self.ssh = self.startSSH(host: host!, username: username!, password: password!)
            print("ssh session: ", self.ssh.isConnected)
            
            // Add asynchronous task to main queue
            DispatchQueue.main.async {
                // Change status label
                self.testConnectionLabel.text = "Connected."
            }

            // Create error pointer
            let error: NSErrorPointer = nil

            // Send enableGPIO command and print response from previous command
            //var response = self.ssh.channel.execute("/lock/enableGPIO", error: error, timeout: 10)
            //print("enableGPIO response: ", response!)

            // Add asynchronous task to main queue
            DispatchQueue.main.async {
                // Change status label
                self.testConnectionLabel.text = "Unlocking..."
            }

            // Send control command, which will unlock lock for five seconds, and print response from previous command
            self.response = self.ssh.channel.execute("/lock/control 5", error: error, timeout: 10)
            print("control response: ", self.response!)

            // Add asynchronous task to main queue
            DispatchQueue.main.async {
                // Change status label
                self.testConnectionLabel.text = "Locked."
            }

            // Send disableGPIO command and print response from previous command
            //response = self.ssh.channel.execute("/lock/disableGPIO", error: error, timeout: 10)
            //print("diableGPIO response: ", response!)
        
            //TO-DO: Put failure option; shown below.
                // ...else change status label to reflect failure
                //testConnectionLabel.text = "Failure"
            
            // End SSH session
            self.endSSH(session: self.ssh)
            
            // Add asynchronous task to main queue
            DispatchQueue.main.async {
                self.testConnectionLabel.text = "Disconnected."
            }
        }
    }
    
    // Test Connection button action
    @IBAction func testConnection(_ sender: AnyObject) {
        // Run testConnectionAction function
        testConnectionAction()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        settings.isHidden = true
    }

    @IBAction func unwindToVC(_ segue: UIStoryboardSegue) {
        settings.isHidden = false
    }

}
